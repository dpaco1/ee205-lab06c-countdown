///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
// Reference time: Tue Jan 21 04:26:07 PM HST 2014
// YEARS: 7 DAYS: 12 HOURS: 6 MINUTES: 20 SECONDS: 57
// YEARS: 7 DAYS: 12 HOURS: 6 MINUTES: 20 SECONDS: 58
// YEARS: 7 DAYS: 12 HOURS: 6 MINUTES: 20 SECONDS: 59
// YEARS: 7 DAYS: 12 HOURS: 6 MINUTES: 21 SECONDS: 0
// YEARS: 7 DAYS: 12 HOURS: 6 MINUTES: 21 SECONDS: 1
// YEARS: 7 DAYS: 12 HOURS: 6 MINUTES: 21 SECONDS: 2
// YEARS: 7 DAYS: 12 HOURS: 6 MINUTES: 21 SECONDS: 3
//
// @author David Paco<dpaco@hawaii.edu>
// @date   24_2_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <math.h>

int main() {
   int ret;
   time_t time_now;
   time_t local_seconds;
   time_t ref_total_seconds;
   struct tm ref;
   struct tm *now;
   char buffer[80];

   ref.tm_year = 2014 - 1900;
   ref.tm_mon = 1 - 1;
   ref.tm_mday = 21;
   ref.tm_hour = 4;
   ref.tm_min = 26;
   ref.tm_sec = 7;
   ref.tm_isdst = -1;
 

   ret = mktime(&ref);
   ref_total_seconds = mktime(&ref);
   printf("Ref Total Seconds = %ld\n", ref_total_seconds);
   if( ret == -1 ) {
      printf("Error: unable to make time using mktime\n");
   } else {
      strftime(buffer, sizeof(buffer), "%a %b %d %X %p %Z %Y", &ref );
      printf("Reference time:  %s\n",buffer);
   }
   while (true) {
      time ( &time_now );
      now = localtime( &time_now );
      local_seconds = time(NULL);
     // printf("Current Local Time in Seconds: %ld\n", local_seconds);
      sleep(1);
      int seconds = (local_seconds - ref_total_seconds)%60;
      int minutes = (((local_seconds - ref_total_seconds)/60)%60);
      int hours = ((local_seconds - ref_total_seconds)/3600)%24;
      printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d \n", now->tm_year-ref.tm_year, now->tm_mday-ref.tm_mday, hours, minutes,seconds);
   
   }
   return(0); 
}
